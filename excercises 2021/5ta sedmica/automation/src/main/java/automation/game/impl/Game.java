package automation.game.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import automation.game.interfaces.Target;


public class Game {
	
	private Logger logger = LoggerFactory.getLogger(Game.class);

	private Hero hero;
	private Target target;

	public Game(Hero hero, Target target) {
		super();
		this.hero = hero;
		this.target = target;
	}

	public void startGame() {
		logger.warn("The game is startiiiiing!");
		while(!target.isDead()) {
			hero.attack(target);
			logger.info("Target was attacked for "+hero.getWeapon().getAttackPoints()+" damage");
		}
		logger.error("The target is dead");
	}
}
