package sabev.test.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.seljup.SeleniumExtension;
@ExtendWith(SeleniumExtension.class)
class OpenBrowser {
	

	 @Test
	    public void testLocalChrome(ChromeDriver driver) throws InterruptedException {
	       driver.get("http://www.mobile.bg");
	       System.out.println(driver.getCurrentUrl());
	       Thread.sleep(10000L);
	    }

	    @Test
	    public void testLocalFirefox(FirefoxDriver driver) throws InterruptedException {
	    	 driver.get("http://www.google.com");
		     System.out.println(driver.getCurrentUrl());
	    }
	    
	    @Test
	    public void testLocalEdge(EdgeDriver driver) throws InterruptedException {
	    	 driver.get("http://www.google.com");
		     System.out.println(driver.getCurrentUrl());
		     Thread.sleep(10000L);
	    }
}
