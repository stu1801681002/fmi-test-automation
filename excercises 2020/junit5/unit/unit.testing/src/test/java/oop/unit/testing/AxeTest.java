package oop.unit.testing;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import impl.Axe;
import impl.Dummy;

public class AxeTest {

	@Test
	public void testAxeLosesDurabilityAfterAtack() {
		Axe axe = new Axe(10, 10);
		assertThat(axe.getDurabilityPoints()).isEqualTo(10);
		Dummy target = new Dummy(10, 10);
		axe.attack(target);
		assertThat(axe.getDurabilityPoints()).isEqualTo(9);
	}
	
	@Test
	public void testAttackWithBrokenWeapon() {
		Axe axe = new Axe(10, 0);
		Dummy target = new Dummy(10, 10);
		try {
		axe.attack(target);
		}catch(IllegalStateException ise){
			assertThat(ise.getMessage()).isEqualTo("Axe is broken.");
		}	
	}
	
}
