package impl;

import java.util.ArrayList;
import java.util.List;

import interfaces.Target;
import interfaces.Weapon;

public class Hero {

    private String name;
    private int experience;
    private Weapon weapon;
    private List<Weapon> weapons;
    
    public Hero(String name, Weapon weapon) {
    	  this.name = name;      
    	  this.experience = 0;  
    	  this.weapon = weapon; 
    	  this.setWeapons(new ArrayList<Weapon>());
    }
    public String getName() {
        return this.name;
    }

    public int getExperience() {
        return this.experience;
    }

    public Weapon getWeapon() {
        return this.weapon;
    }

    public void attack(Target target) {
        this.weapon.attack(target);

        if (target.isDead()) {
            this.experience += target.giveExperience();
        }
    }
	public List<Weapon> getWeapons() {
		return weapons;
	}
	public void setWeapons(List<Weapon> weapons) {
		this.weapons = weapons;
	}
}
