package oop.unit.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	AxeTest.class,
	TargetTest.class,
	HeroTest.class
})
public class RunAll {

}
