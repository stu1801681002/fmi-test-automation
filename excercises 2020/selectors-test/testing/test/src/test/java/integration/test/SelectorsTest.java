package integration.test;

import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelectorsTest {
	private WebDriver driver;

	@Before
	public void before() throws IOException {
		URL url = Thread.currentThread().getContextClassLoader().getResource("chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", new File(url.getFile()).getCanonicalPath());
		driver = new ChromeDriver();
		driver.get("https://www.google.com/");	 
	}

	@Test
	public void testSelectors() {
		assertThat(driver.findElement(By.cssSelector("#hplogo")).getAttribute("alt"), Matchers.equalTo("Google"));
	}
	
	@After
	public void after() {
		driver.close();
		driver.quit();
	}
}
