package oop.unit.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.TestInfo;

public class Junit5Test {
	@RepeatedTest(value = 3, name = RepeatedTest.LONG_DISPLAY_NAME)
	@DisplayName("User Test")
	void repeatedTest(TestInfo testInfo) {
	    System.out.println("Executing repeated test : "+testInfo.getDisplayName());
	  
	    assertEquals(2, Math.addExact(1, 1), "1 + 1 should equal 2");
	}
}
