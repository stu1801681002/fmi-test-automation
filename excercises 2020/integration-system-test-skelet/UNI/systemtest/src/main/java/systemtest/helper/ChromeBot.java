package systemtest.helper;

import static systemtest.helper.WebConfig.BASE_URL;
import static java.lang.Boolean.TRUE;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeBot extends TestWatcher {

	public ChromeBot() {
		this(TRUE);
	}
	
	public ChromeBot(boolean defaultUser) {
	
	}
	protected WebDriver driver;
	
	public WebDriver getDriver() {
		return driver;
	}

	@Override
	protected void starting(Description description) {
		try {
			File file = new File("*\\resources\\chromedriver.exe");
			String urlGenerated = file.getAbsolutePath();
			String filepath= urlGenerated.replace("\\*\\","\\");
			System.setProperty("webdriver.chrome.driver",
					filepath);
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get(BASE_URL);
			/*login*/
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException("Startup page not accessible!");
		}
	}
	
	@Override
	protected void finished(Description description) {
		try {
			driver.quit();
		} catch (Exception e) {
			throw new java.lang.IllegalStateException("Can't destroy Selenium", e);
		}
	}	
}
