package systemtest.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Function;

public class WebDriverUtils {
	
	private static final Log LOG = LogFactory.getLog(WebDriverUtils.class);
	
	private WebDriverUtils() {
		// cannot be instantiated
	}
	
	public static boolean isElementPresentOnPage(WebDriver driver, By by){
		int count = driver.findElements(by).size();
		if(count > 1){
			throw new IllegalArgumentException("The element is found more than once. Please give more specific locator!");
		}
		boolean isPresent = count==1 ?  true :  false;
		return isPresent;
	}
	
	public static void selectAndWait(WebDriver driver, By by, String visibleText) {
		final int oldHash = findHtmlTag(driver).hashCode();
		LOG.debug("Old <html> hashcode: " + oldHash);
		
		new Select(driver.findElement(by)).selectByVisibleText(visibleText);
		WebDriverUtils.acceptAlertIfPresent(driver);
		waitForNewHtmlTag(driver, oldHash);	
	}
	
	private static void waitForNewHtmlTag(WebDriver driver, final int oldHash) {
		WebDriverUtils.acceptAlertIfPresent(driver);
		new WebDriverWait(driver, 30, 200).until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver input) {
				int newHash = findHtmlTag(input).hashCode();
				LOG.debug("New <html> hashcode: " + newHash);
				return newHash != oldHash;
			}
		});
	}
	
	private static WebElement findHtmlTag(WebDriver driver) {
		return driver.findElement(By.tagName("html"));
	}

	public static void selectElement(WebDriver driver, By by) {
	Boolean isSelected  = driver.findElement(by).isSelected();
	if(isSelected){
		LOG.debug("The element "+ by +" is already selected");
		return;
	  }
	driver.findElement(by).click();
	}
	
	public static void sleepFor(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
    public static boolean acceptAlertIfPresent(WebDriver driver) {
		
		boolean presentFlag = false;
		try {
			Alert alert = driver.switchTo().alert();
			presentFlag = true;
			alert.accept();
		} catch (NoAlertPresentException ex) {
			//ex.printStackTrace();
		}
		return presentFlag;
	}
}
