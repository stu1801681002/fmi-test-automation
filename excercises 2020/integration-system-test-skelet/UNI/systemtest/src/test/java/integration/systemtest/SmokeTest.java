package integration.systemtest;

import static org.junit.Assert.assertThat;
import java.io.IOException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import systemtest.helper.ChromeBot;
import systemtest.helper.WebDriverUtils;

public class SmokeTest {

	@Rule
	public ChromeBot chromeBot = new ChromeBot();
	
	private WebDriver driver;

	@Before
	public void setup() throws IOException {
		driver = chromeBot.getDriver();
	}
	
	@Test
	public void testPageIsLoadedSuccessfully() throws IOException {
		assertThat(WebDriverUtils.isElementPresentOnPage(driver, By.xpath("//img[@alt='eBay Logo']")),Matchers.is(true));
	}
	
	
	
}
